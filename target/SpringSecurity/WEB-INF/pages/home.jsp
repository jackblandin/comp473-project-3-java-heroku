<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <nav>
            <ul class="nav nav-pills">
                <li role="presentation" class="active"><a href="/">Home</a></li>
                <li role="presentation"><a href="facility">Facility</a></li>
                <li role="presentation"><a href="unit">Units</a></li>
                <li role="presentation"><a href="inspection">Inspections</a></li>
                <li role="presentation"><a href="maintenance">Maintenance</a></li>
            </ul>
        </nav>
        <div classa="page-header">
            <h2>${title}</h2>
        </div>
        <br />
    </div>

    <style>
        * {
            text-align:center;
        }
        nav .row {
            font-size:24pt;
            line-height:40pt;
        }
        html, body {
            background:#eee;
        }
        .container {
            padding: 10px 20px;
        }
        nav {
            background:#dadada;
            border-radius: 2px;4px;
            border:1px solid #ccc;
        }
        .page-header {
            text-align: center;
            margin-top: 30px;
            margin-bottom: 20px;
        }
    </style>
</body>
</html>