<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: jackblandin
  Date: 3/1/15
  Time: 1:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page session="true" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


</head>
<body>
    <div class="container">
        <nav>
            <ul class="nav nav-pills">
                <li role="presentation"><a href="/">Home</a></li>
                <li role="presentation" class="active"><a href="facility">Facility</a></li>
                <li role="presentation"><a href="unit">Units</a></li>
                <li role="presentation"><a href="inspection">Inspections</a></li>
                <li role="presentation"><a href="maintenance">Maintenance</a></li>
            </ul>
        </nav>
        <div class="page-header">
            <h2>${title}</h2>
        </div>
        <br />
        <div>
            <h4>Search Facility</h4>
            <form:form method="get" action="facility/search">
                <div class="form-group">
                    <label for="name">Name</label>
                    <form:input path="name" type="search"></form:input>
                    <input type="submit" action="Submit" value="Go" type="submit" />
                </div>
            </form:form>
        </div>
        <br />
        <div class="results">
            <h4>Results</h4>
            <table class="table-bordered table" >
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${f1.getId()}</td>
                        <td>${f1.getName()}</td>
                        <td>${f1.getAddress()}</td>
                        <td>${f1.getDetail()}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br />
        <div>
            <h4>
                Add Facility
                <button type="button" class="toggle-addNew btn btn-sm" aria-label="add new">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
            </h4>
            <form:form method="post" action="addFacility" class="form-addNew">
                <div class="form-group">
                    <label for="name">Name</label>
                    <form:input path="name" type="text" class="form-control"></form:input>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <form:input id="detail" path="address" type="text" class="form-control" ></form:input>
                </div>
                <div class="form-group">
                    <label for="detail">Detail</label>
                    <form:input id="detail" path="detail" type="text" class="form-control"></form:input>
                </div>
                <input type="submit" action="Submit" />
            </form:form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".form-addNew").hide();
            $(".toggle-addNew").click(function() {
                $(".form-addNew").slideToggle(200);
            });
            if (${f1.getId()} == 0) {
                $(".results").hide();
            }
        });
    </script>
    <style>
        html, body { background:#eee;}
        .container {
            padding: 10px 20px;
        }
        nav {
            background:#dadada;
            border-radius: 2px;4px;
            border:1px solid #ccc;
        }
        .page-header {
            text-align: center;
            margin-top: 30px;
            margin-bottom: 20px;
        }
    </style>

</body>
</html>
