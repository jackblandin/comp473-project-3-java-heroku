<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: jackblandin
  Date: 3/4/15
  Time: 12:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <title>${title}</title>
</head>
<body>
    <div class="container">
        <nav>
            <ul class="nav nav-pills">
                <li role="presentation"><a href="/">Home</a></li>
                <li role="presentation" class="active"><a href="facility">Facility</a></li>
                <li role="presentation"><a href="unit">Units</a></li>
                <li role="presentation"><a href="inspection">Inspections</a></li>
                <li role="presentation"><a href="maintenance">Maintenance</a></li>
            </ul>
        </nav>
        <div classa="page-header">
            <h2>${title}</h2>
        </div>
        <br />
        <div>
            <h3>Page Description</h3>
            <p>${description}</p>
        </div>
        <table class="table-striped table-bordered table-hover table">
            <tbody>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Link</th>
                </tr>
            <c:forEach var="f" items="${facilities}" >
                <tr>
                    <td>${f.getId()}</td>
                    <td>${f.getName()}</td>
                    <td><a href="facility?id=${f.getId()}">http://localhost:8080/facility?id=${f.getId()}</a></td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
    <style>
        html, body { background:#eee;}
        .container {
            padding: 10px 20px;
        }
        nav {
            background:#dadada;
            border-radius: 2px;4px;
            border:1px solid #ccc;
        }
        .page-header {
            text-align: center;
            margin-top: 30px;
            margin-bottom: 20px;
        }
    </style>
</body>
</html>
