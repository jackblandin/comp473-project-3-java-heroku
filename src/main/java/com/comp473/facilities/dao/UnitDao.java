package com.comp473.facilities.dao;

import com.comp473.facilities.model.Unit;
import java.util.List;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public interface UnitDao {
    Unit getUnit(int id);
    List<Unit>getAllUnits(int facilityId);
    void insertUnit(Unit u);
    Unit searchUnit(int number);

}
