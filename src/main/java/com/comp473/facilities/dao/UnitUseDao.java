package com.comp473.facilities.dao;

import com.comp473.facilities.model.UnitUse;
import java.util.List;
import java.util.Date;

/**
 * Created by dalestoutjr on 3/15/15.
 */
public interface UnitUseDao {
    UnitUse getUnitUse(int facilityId);
    List<UnitUse> getAllUnitUses(int facilityId);
    void insertUnitUse(UnitUse uu);
    List<Integer> isInUseDuringInterval(Date df, Date dt, int facilityId);
    void vacateUnit(int unitUseId);
    List<Integer> listActualUsage(int facilityId);
    double calculateUsageRate(int facilityId);

}
