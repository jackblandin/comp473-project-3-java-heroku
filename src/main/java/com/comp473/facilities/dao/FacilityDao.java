package com.comp473.facilities.dao;

import com.comp473.facilities.model.Facility;


import java.util.List;

/**
 * Created by jackblandin on 3/1/15.
 */
public interface FacilityDao {
    Facility getFacility(int id);
    List<Facility> getAllFacilities();
    void insertFacility(Facility f);
    Facility searchFacility(String name);
    int requestAvailableCapacity(int facilityId);


}

