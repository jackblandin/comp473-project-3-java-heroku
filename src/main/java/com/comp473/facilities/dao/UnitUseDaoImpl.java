package com.comp473.facilities.dao;

import com.comp473.facilities.model.Facility;
import com.comp473.facilities.model.Unit;
import com.comp473.facilities.model.UnitUse;

import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.Iterator;
import java.util.List;
import java.util.Date;
import java.util.ArrayList;

/**
 * Created by dalestoutjr on 3/15/15.
 */
public class UnitUseDaoImpl implements UnitUseDao {
    private SessionFactory sessionFactory;
     public UnitUse getUnitUse(int id){
         try{
             Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
             session.beginTransaction();

             Query getUnitUseQuery = session.createQuery("FROM UnitUse WHERE id=:id");
             getUnitUseQuery.setInteger("id", id);

             List unitUses = getUnitUseQuery.list();
             session.getTransaction().commit();

             return (UnitUse)unitUses.get(0);

         } catch(Exception e){
             e.printStackTrace();
         }
         return null;
     }

    public List<UnitUse> getAllUnitUses(int facilityId){
        try{
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getAllUnitUsesQuery = session.createQuery("FROM UnitUse INNER JOIN Unit ON UnitUse.unitsId=Unit.id " +
                    "INNER JOIN Facility ON Unit.facilityId=:facilityId");
            getAllUnitUsesQuery.setInteger("facilityId", facilityId);

            List unitUses = getAllUnitUsesQuery.list();
            session.getTransaction().commit();

            return (List<UnitUse>)unitUses;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void insertUnitUse(UnitUse uu){
        Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(uu);
        session.getTransaction().commit();
    }

    public List<Integer> isInUseDuringInterval(Date df, Date dt, int facilityId){
        List<Integer> unitIdList = new ArrayList<Integer>();
        List<UnitUse> unitUseList = getAllUnitUses(facilityId);
        Iterator <UnitUse> listIterator = unitUseList.iterator();
        while(listIterator.hasNext()){
            UnitUse tempUnitUse = listIterator.next();
            if(tempUnitUse.getDateFrom().after(df) && tempUnitUse.getDateTo().before(dt) && tempUnitUse.getIsInUse()){
                unitIdList.add(tempUnitUse.getUnitsId());
            }
        }
        return unitIdList;
    }

    public void vacateUnit(int unitUseId){
        getUnitUse(unitUseId).setIsInUse(false);
    }

    public List<Integer> listActualUsage(int facilityId){
        List<Integer> unitIdList = new ArrayList<Integer>();
        List<UnitUse> unitUseList = getAllUnitUses(facilityId);
        Iterator<UnitUse> listIterator = unitUseList.iterator();
        while(listIterator.hasNext()){
            UnitUse tempUnitUse = listIterator.next();
            if (tempUnitUse.getIsInUse()){
                unitIdList.add(tempUnitUse.getUnitsId());
            }
        }
        return unitIdList;
    }

    public double calculateUsageRate(int facilityId){
        double totalIsInUse = 0;
        double totalUnits = 0;
        List<UnitUse> unitUseList = getAllUnitUses(facilityId);
        Iterator<UnitUse> listIterator = unitUseList.iterator();
        while(listIterator.hasNext()){
            UnitUse tempUnitUse = listIterator.next();
            if (tempUnitUse.getIsInUse()){
                totalIsInUse ++;
                totalUnits ++;
            }
            else{
                totalUnits ++;
            }
        }
        if(totalUnits>0){
            return totalIsInUse/totalUnits;
        }
        else return 0;
    }
}
