package com.comp473.facilities.dao;

import com.comp473.facilities.model.User;

public interface UserDao {

	User findByUserName(String username);

}