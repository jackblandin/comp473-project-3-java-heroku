package com.comp473.facilities.dao;

import com.comp473.facilities.model.Facility;
import com.comp473.facilities.model.UnitUse;
import com.comp473.facilities.model.Unit;
import com.comp473.facilities.dao.UnitUseDao;
import com.comp473.facilities.dao.UnitDaoImpl;


import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import java.util.Iterator;

/**
 * Created by jackblandin on 3/1/15.
 */
public class FacilityDaoImpl implements FacilityDao {
    private SessionFactory sessionFactory;

    public Facility getFacility(int id) {
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getFacilityQuery = session.createQuery("From Facility where id=:id");
            getFacilityQuery.setInteger("id", id);

            List facilities = getFacilityQuery.list();
            session.getTransaction().commit();

            return (Facility)facilities.get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Facility searchFacility(String name) {
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getFacilityQuery = session.createQuery("From Facility where name=:name");
            getFacilityQuery.setString("name", name);

            List facilities = getFacilityQuery.list();
            session.getTransaction().commit();

            return (Facility)facilities.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Facility> getAllFacilities() {
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getAllFacilitiesQuery = session.createQuery("From Facility");

            List facilities = getAllFacilitiesQuery.list();
            session.getTransaction().commit();

            return (List<Facility>)facilities;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertFacility(Facility f) {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(f);
            session.getTransaction().commit();
    }

    public int requestAvailableCapacity(int facilityId){
        try{
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();
            int totalCapacity = 0;
            Query getAllUnitUsesQuery = session.createQuery("FROM UnitUse INNER JOIN Unit ON UnitUse.unitsId=Unit.id " +
                    "INNER JOIN Facility ON Unit.facilityId=:facilityId");
            getAllUnitUsesQuery.setInteger("facilityId", facilityId);

            List unitUses = getAllUnitUsesQuery.list();
            session.getTransaction().commit();

            Iterator <UnitUse> listIterator = unitUses.listIterator();
            while (listIterator.hasNext()){
                UnitUse tempUnitUse = listIterator.next();
                totalCapacity += tempUnitUse.getCapacity();
            }
            return totalCapacity;

        } catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }


}
