package com.comp473.facilities.dao;

import com.comp473.facilities.model.Facility;
import com.comp473.facilities.model.MaintenanceRequest;
import com.comp473.facilities.model.Unit;
import com.comp473.facilities.model.UnitUse;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.Iterator;
import java.util.List;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public class UnitMaintenanceDaoImpl implements UnitMaintenanceDao {
    private SessionFactory sessionFactory;

    public MaintenanceRequest getMaintenanceRequest(int id){
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getMaintenanceRequestsQuery = session.createQuery("From MaintenanceRequest where id=:id");
            getMaintenanceRequestsQuery.setInteger("id", id);

            List maintenanceRequests = getMaintenanceRequestsQuery.list();
            session.getTransaction().commit();

            return (MaintenanceRequest)maintenanceRequests.get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public  List<MaintenanceRequest> getAllMaintenanceRequests(int facilityId) {
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getMaintenanceRequestQuery = session.createQuery("FROM MaintenanceRequest INNER JOIN Unit ON MaintenanceRequest.unitsId=Unit.id " +
                    "INNER JOIN Facility ON Unit.facilityId=:facilityId");
            getMaintenanceRequestQuery.setInteger("facilityId", facilityId);

            List maintenanceRequests = getMaintenanceRequestQuery.list();
            session.getTransaction().commit();

            return (List<MaintenanceRequest>)maintenanceRequests;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertMaintenanceRequest(MaintenanceRequest mr){
        Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(mr);
        session.getTransaction().commit();
    }

    public double calculateMaintenanceCost(int facilityId){
        double totalMaintenanceCost = 0;
        List<MaintenanceRequest> maintenanceRequestsList = getAllMaintenanceRequests(facilityId);
        Iterator<MaintenanceRequest> listIterator = maintenanceRequestsList.iterator();
        while(listIterator.hasNext()){
            totalMaintenanceCost += listIterator.next().getMaintenanceCost();
        }
        return totalMaintenanceCost;
    }

    public int calculateProblemRateForFacility(int facilityId){
        List<MaintenanceRequest> maintenanceRequestList = getAllMaintenanceRequests(facilityId);
        int totalMaintenanceRequest = maintenanceRequestList.size();
        int totalUnitsInFacility = 0;

        try{
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getAllUnitsQuery = session.createQuery("FROM Unit where facilityId=:fk_facilities");

            List units = getAllUnitsQuery.list();
            session.getTransaction().commit();
            totalUnitsInFacility = units.size();

        } catch (Exception e){
            e.printStackTrace();
        }
        if(totalUnitsInFacility>0){
            return totalMaintenanceRequest/totalUnitsInFacility;
        }
        else return 0;

    }
}
