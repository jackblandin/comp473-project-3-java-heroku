package com.comp473.facilities.dao;

import com.comp473.facilities.model.Unit;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public class UnitDaoImpl implements UnitDao {

    private SessionFactory sessionFactory;

    public Unit getUnit(int id){
        try{
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getUnitQuery = session.createQuery("FROM Unit where id=:id");
            getUnitQuery.setInteger("id", id);

            List units = getUnitQuery.list();
            session.getTransaction().commit();

            return (Unit)units.get(0);
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;

    }

    public Unit searchUnit(int number) {
        try {
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();

            Query getUnitQuery = session.createQuery("FROM Unit where number=:number");
            getUnitQuery.setInteger(number, number);

            List units = getUnitQuery.list();
            session.getTransaction().commit();

            return (Unit) units.get(0);
        } catch (Exception e){
            e.printStackTrace();

        }
        return null;

    }

    public List<Unit> getAllUnits(int facilityId){
        try{
            Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getAllUnitsQuery = session.createQuery("FROM Unit where facilityId=:fk_facilities");

            List units = getAllUnitsQuery.list();
            session.getTransaction().commit();

            return (List<Unit>)units;

        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void insertUnit(Unit u){
        Session session = HibernatePGSQLHelper.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(u);
        session.getTransaction().commit();
    }


}
