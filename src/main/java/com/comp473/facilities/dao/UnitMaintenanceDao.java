package com.comp473.facilities.dao;

import com.comp473.facilities.model.MaintenanceRequest;

import java.util.List;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public interface UnitMaintenanceDao {
    MaintenanceRequest getMaintenanceRequest(int id);
    List<MaintenanceRequest> getAllMaintenanceRequests(int facilityId);
    void insertMaintenanceRequest(MaintenanceRequest mr);
    double calculateMaintenanceCost(int facilityId);
    int calculateProblemRateForFacility(int facilityId);
}
