package com.comp473.facilities.model;

import java.util.Date;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public class UnitUse {
    private int id;
    private int unitsId;
    private String unitUser;
    private int capacity;
    private String inspection;
    private double rent;
    private String useDescription;
    private Date dateFrom;
    private Date dateTo;
    private boolean isInUSe;

    public UnitUse(){}

    public UnitUse(String unitUser, int capacity, String useDescription, Date dateFrom, Date dateTo){
        this.unitUser = unitUser;
        this.capacity = capacity;
        this.useDescription = useDescription;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.isInUSe = true;
    }

    public void setId(int id){ this.id = id;}
    public void setUnitsId(int unitsId){ this.unitsId = unitsId; }
    public void setUnitUser(String unitUser){ this.unitUser = unitUser; }
    public void setCapacity(int capacity){ this.capacity = capacity; }
    public void setInspection(String inspection){ this.inspection = inspection;}
    public void setRent(double rent){ this.rent = rent; }
    public void setUseDescription(String useDescription){ this.useDescription = useDescription; }
    public void setDateFrom(Date dateFrom){ this.dateFrom = dateFrom; }
    public void setDateTo(Date dateTo){this.dateTo = dateTo; }
    public void setIsInUse(boolean isInUSe){ this.isInUSe = isInUSe; }

    public int getId(){ return id;}
    public int getUnitsId(){ return unitsId; }
    public String getUnitUser(){ return unitUser; }
    public int getCapacity(){ return getCapacity(); }
    public String getInspection() { return inspection; }
    public double getRent() {return rent; }
    public String getUseDescription() { return useDescription; }
    public Date getDateFrom(){ return dateFrom; }
    public Date getDateTo(){ return dateTo; }
    public boolean getIsInUse(){ return isInUSe; }



}
