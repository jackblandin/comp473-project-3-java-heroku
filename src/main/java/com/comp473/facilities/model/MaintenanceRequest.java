package com.comp473.facilities.model;

import java.util.Date;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public class MaintenanceRequest {
    private int id;
    private int unitsId;
    private String description;
    private Date dateFrom; // Date maintenance is requested.
    private Date dateTo; // Latest Date maintenance needs to be completed.
    private String maintenanceDescription;
    private Date dateStart; //Actual maintenance start date.
    private Date dateEnd; // Actual maintenance end date.
    private String scope;
    private boolean isMaintenanceCompleted;
    private double maintenanceCost;

    public MaintenanceRequest(){}

    public MaintenanceRequest(Date dateFrom, Date dateTo, String description, Date dateStart, Date dateEnd, String scope, double maintenanceCost){
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.scope = scope;
        this.isMaintenanceCompleted = false;
        this.maintenanceCost = maintenanceCost;

    }

    public void setId(int id){ this.id = id; }
    public void setUnitsId(int unitsId){ this.unitsId = unitsId; }
    public void setDescription(String description){ this.description = description; }
    public void setDateFrom(Date dateFrom){ this.dateFrom = dateFrom; }
    public void setDateTo(Date dateTo){ this.dateTo = dateTo; }
    public void setMaintenanceDescription(String maintenanceDescription){ this.maintenanceDescription = maintenanceDescription; }
    public void setDateStart(Date dateStart){ this.dateStart = dateStart; }
    public void setDateEnd(Date dateEnd){ this.dateEnd = dateEnd; }
    public void setScope(String scope){ this.scope = scope; }
    public void setIsMaintenanceCompleted(boolean isMaintenanceCompleted){ this.isMaintenanceCompleted = isMaintenanceCompleted; }
    public void setMaintenanceCost(double maintenanceCost){ this.maintenanceCost = maintenanceCost; }

    public int getId(){ return id; }
    public int getUnitsId(){ return unitsId; }
    public String getDescription(){ return description; }
    public Date getDateFrom(){ return dateFrom; }
    public Date getDateTo(){ return dateTo; }
    public String getMaintenanceDescription(){ return maintenanceDescription; }
    public Date getDateStart(){ return dateStart; }
    public Date getDateEnd(){ return dateEnd; }
    public String getScope(){ return scope; }
    public boolean getIsMaintenanceCompleted(){ return isMaintenanceCompleted; }
    public double getMaintenanceCost(){ return maintenanceCost; }

}
