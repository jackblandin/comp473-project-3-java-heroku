package com.comp473.facilities.model;

/**
 * Created by jackblandin on 3/1/15.
 */
public class Facility {

    private int id;
    private String name;
    private String address;
    private String detail;

    public Facility() { }

    public Facility(String name) {
        this.name = name;
    }

    public Facility(String name, String address, String detail){
        this.name = name;
        this.address = address;
        this.detail = detail;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getAddress() { return address; }
    public void setAddress(String address){ this.address = address; }
    public String getDetail(){return detail;}
    public void setDetail(String detail){this.detail = detail;}


}
