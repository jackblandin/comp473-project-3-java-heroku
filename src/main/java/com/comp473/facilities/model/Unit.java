package com.comp473.facilities.model;

/**
 * Created by dalestoutjr on 3/14/15.
 */
public class Unit {
    private int id;
    private int facilityId;
    private int number;

    public Unit(){}

    public Unit(int number){
        this.number = number;
    }

    public int getId(){ return id;}
    public void setId(int id) { this.id = id; }
    public int getFacilityId(){ return facilityId; }
    public void setFacilityId(int facilityId){ this.facilityId = facilityId; }
    public int getNumber(){ return number; }
    public void setNumber( int number){ this.number = number; }

}
