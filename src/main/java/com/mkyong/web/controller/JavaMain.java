package com.mkyong.web.controller;

import com.comp473.facilities.dao.FacilityDao;
import com.comp473.facilities.dao.UnitDao;
import com.comp473.facilities.dao.UnitDaoImpl;
import com.comp473.facilities.dao.FacilityDaoImpl;
import com.comp473.facilities.model.Facility;
import com.comp473.facilities.model.Unit;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Scanner;

/**
 * Created by jackblandin on 3/1/15.
 */
public class JavaMain {

    private static ApplicationContext context = new ClassPathXmlApplicationContext("file:src/main/webapp/WEB-INF/spring-database.xml");
    private static FacilityDao facilityDao = new FacilityDaoImpl();
    private static UnitDao unitDao = new UnitDaoImpl();
    private static Scanner scanner = new Scanner(System.in);

    public static void main (String args[]) throws Exception {
        mainLoop:
        while (true) {

            System.out.println("\n0\tquit");
            System.out.println();
            System.out.println("1\tgetFacility(int id)");
            System.out.println("2\tgetAllFacilities()");
            System.out.println("3\taddFacility(Facility facility)");
            System.out.println();
            System.out.println("4\tgetUnit(int id)");
            System.out.println("5\tgetAllUnits(int facilityId)");
            System.out.println("6\taddUnit(Unit unit)");

            System.out.print("\nEnter function:\t");
            int func = scanner.nextInt();
            switch (func) {
                case 0:
                    System.out.println("\nQUITTING\n");
                    break mainLoop;
                case 1:
                    getFacility();
                    break;
                case 2:
                    getAllFacilities();
                    break;
                case 3:
                    addFacility();
                    break;
                case 4:
                    getUnit();
                default:
                    System.out.println("Not a function");
                    break;
            }
        }
    }

    public static void getFacility() {
        System.out.print("Facility id:\t");
        int facilityId = scanner.nextInt();
        try {
            Facility facility = facilityDao.getFacility(facilityId);
            System.out.println("\n***** QUERY RESULTS ******\n");
            System.out.println("facility id:\t" + facility.getId());
            System.out.println("facility name:\t" + facility.getName());
            System.out.println("facility address:\t" + facility.getAddress());
            System.out.println("facility detail:\t" + facility.getDetail());
            System.out.println("\n***** END OF RESULTS ******\n");
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("\nERROR:\tNo Facility exists with id: " + facilityId);
        }

    }

    public static void getAllFacilities() {
        try {

            List<Facility> facilityList= facilityDao.getAllFacilities();
            System.out.println("\n***** QUERY RESULTS ******\n");
            for(Facility f: facilityList) {
                System.out.println("\nfacility id:\t" + f.getId());
                System.out.println("facility name:\t" + f.getName());
                System.out.println("facility address:\t" + f.getAddress());
                System.out.println("facility detail:\t" + f.getDetail());

            }
            System.out.println("\n***** END OF RESULTS ******\n");

        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void addFacility() {
        Scanner scanner2 = new Scanner(System.in);
        Facility facility = new Facility();
        System.out.println("Facility Name:");
        String facilityName = scanner2.nextLine();
        facility.setName(facilityName);
        System.out.println("Facility Address:");
        String facilityAddress = scanner2.nextLine();
        facility.setAddress(facilityAddress);
        System.out.println("Facility Detail:");
        String facilityDetail = scanner2.nextLine();
        facility.setDetail(facilityDetail);
        facilityDao.insertFacility(facility);

    }

    public static void getUnit() {
        System.out.print("Unit id:\t");
        int unitId = scanner.nextInt();
        try {
            Unit unit = unitDao.getUnit(unitId);
            System.out.println("\n***** QUERY RESULTS ******\n");
            System.out.println("unit id:\t" + unit.getId());
            System.out.println("Unit facility id:\t" + unit.getFacilityId());
            System.out.println("facility unit number:\t" + unit.getNumber());
            System.out.println("\n***** END OF RESULTS ******\n");
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("\nERROR:\tNo Unit exists with id: " + unitId);
        }

    }
}
