package com.mkyong.web.controller;

import javax.servlet.http.HttpServletRequest;

import com.comp473.facilities.dao.FacilityDao;
import com.comp473.facilities.dao.FacilityDaoImpl;
import com.comp473.facilities.model.Facility;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    private ApplicationContext context = new ClassPathXmlApplicationContext("file:/Users/jackblandin/Desktop/spring_hib_facilities/src/main/webapp/WEB-INF/spring-database.xml");
//    private ApplicationContext context = new ClassPathXmlApplicationContext("spring-database.xml");
    private FacilityDao facilityDao = new FacilityDaoImpl();

	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring + Hibernate Facilities");
		model.setViewName("home");
		return model;

	}

    @RequestMapping(value = "/facilities")
    public ModelAndView getAllFacilities() {
        ModelAndView model = new ModelAndView("facilities", "command", new ArrayList<Facility>());
        model.addObject("title", "Facilities");
        model.addObject("description",
                "This page displays the information for all Facilities in the database.");
        List<Facility> facilities = facilityDao.getAllFacilities();
        model.addObject("facilities", facilities);
        return model;
    }

    @RequestMapping(value = "/addFacility", method = RequestMethod.POST)
    public String addFacility(@ModelAttribute("facility")
                             Facility facility, BindingResult result) {

        facilityDao.insertFacility(facility);
        return "redirect:facility";
    }

    @RequestMapping("/facility")
    public ModelAndView getFacility(@RequestParam(value = "id", defaultValue="0") int id) {
        ModelAndView model = new ModelAndView("facility", "command", new Facility());
        model.addObject("title", "Facility Page");
        model.addObject("description",
                "This page displays the information for a specific facility. " +
                "E.g. 'http://localhost:8080/getFacility?id=4' will retrieve the Facility with id = 4." +
                "It also provides a form for adding a new Facility.");

        Facility f1 = new Facility();
        if (id != 0) {
             f1 = facilityDao.getFacility(id);
        }
        model.addObject("f1", f1);

        return model;
    }

    @RequestMapping(value="/facility/search", method=RequestMethod.GET)
    public String searchFacility(@RequestParam(value="name") String name) {
        Facility f_search = facilityDao.searchFacility(name);
        return "redirect:/facility?id=" + Integer.toString(f_search.getId());
    }


}